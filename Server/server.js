const {RSA} = require("./rsaServer.js");

const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const HOST = "localhost";
const PORT = 3444;

let rsa = new RSA();
var keys = [];

server.listen(PORT, HOST, function() {

  console.log("-----------------------------------------------------");
  console.log("Server listen on http://" + HOST + ":" + PORT);
  console.log("-----------------------------------------------------");

  console.log("Server crypt keys ");
  console.log(rsa.openKeys);

  console.log("Server decrypt keys ");
  console.log(rsa.closeKeys);

  console.log("-----------------------------------------------------");
});

io.on('connection', client => {

  ServerMessageOnConnection(client);

  //Send crypt keys to user
  client.emit("serverOpenKeys", JSON.stringify(rsa.openKeys));

  //Get user crypt keys
  client.on('clientOpenKeys', data => {

      var jsonData = JSON.parse(data);
      var clientKeys = jsonData;

      addKeyToArray(client.handshake.query.token, clientKeys);
  });

  client.on("registerUser", data => {
    console.log("User registered");
    var decryptedUserData = DecryptUserData(JSON.parse(data), client.handshake.query.token);

    //Проверка на валидность регистрации
    client.emit("registerConfirm", "1");
  });

});

function DecryptUserData(user, token)
{
    var keys = FindUserKeysByToken(token);

    user.Login = rsa.DecryptString(user.Login);
    user.Password = rsa.DecryptString(user.Password);

    return user;
}

function FindUserKeysByToken(token)
{
  return keys[token];
}

function ServerMessageOnConnection(client)
{
  console.log("-----------------------------------------------------");
  console.log("Connected");
  console.log("Client token : " + client.handshake.query.token);
  console.log("-----------------------------------------------------");
}

function addKeyToArray(token, clientKeys)
{
  keys.push(
    {
      token : token,
      keys : clientKeys
    }
  );
}
