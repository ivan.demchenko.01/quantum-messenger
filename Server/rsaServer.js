class RSA {
    openKeys = {
        e : 0,
        n : 0
    }

    closeKeys = {
        d : 0,
        n : 0
    }

    constructor()
    {
        var primeNumbers = this.GetPrimeNumbers();

        var n, m, d, e;

        n = primeNumbers.p * primeNumbers.q;
        m = (primeNumbers.p - 1) * (primeNumbers.q - 1);
        d = primeNumbers.d;
        e = this.FindE(d, m);

        this.openKeys.e = e;
        this.openKeys.n = n;

        this.closeKeys.d = d;
        this.closeKeys.n = n;

        //console.log(this.openKeys);
       // console.log(this.closeKeys);
    }


    CryptString(str, openKeys)
    {
        var e = openKeys.e;
        var n = openKeys.n;

        var cryptedKeys = "";

        for (var i = 0; i < str.length; i++) {
            var bi = BigInt(str.charCodeAt(i));

            bi = bi ** BigInt(e);
            bi = bi % BigInt(n);

            cryptedKeys += bi + ';';
          }

          return cryptedKeys;
    }

    DecryptString(listCodes)
    {
        var decryptedString = "";
        
        var codes = getArrayFromString(listCodes);

        for (var i = 0; i < codes.length; i++) {
            var bi = BigInt(codes[i]);

            bi = bi ** BigInt(this.closeKeys.d);
            bi = bi % BigInt(this.closeKeys.n);

            decryptedString += String.fromCharCode(Number(bi));
          }

          return decryptedString;
    }



    FindE(d, m)
    {
        var i = 1;
        var e = 0;

        while (e == 0)
        {
            if ((i * d) % m == 1)
                e = i;
            i++;
        }

        return e;
    }

    GetPrimeNumbers ()
    {
        var min = getRandomArbitrary(15, 100);
        var max = getRandomArbitrary(min, 200);

        var primeNumbers = [];

        for (var i = min; i < max; i++)
            {
                var isPrime = true;
                
                for (var j = 1; j < i; j++)
                    if (i != j && j != 1) 
                        if (i % j == 0)
                        {
                            isPrime = false;
                            break;
                        } 

                        

                if (isPrime)
                    {
                        primeNumbers.push(i);
                    }
            }

        var pIndex = 0, qIndex = 0, dIndex = 0;

        do
        {
            pIndex = getRandomArbitrary(0, primeNumbers.length);
            qIndex = getRandomArbitrary(0, primeNumbers.length);
            dIndex = getRandomArbitrary(0, primeNumbers.length);
        }
        while(qIndex == pIndex || qIndex == dIndex || pIndex == dIndex);

        return {
            p : primeNumbers[pIndex],
            q : primeNumbers[qIndex],
            d : primeNumbers[dIndex]
        };
    }
}

module.exports = { RSA }

function getRandomArbitrary(min, max) {
    return Math.floor( Math.random() * (max - min) + min);
  }

  function getArrayFromString(string, sep = ";")
  {
      return string.split(sep);
  }