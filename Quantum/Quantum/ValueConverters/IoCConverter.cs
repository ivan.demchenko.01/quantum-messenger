﻿using Ninject;
using Quantum.Core;
using System;
using System.Diagnostics;
using System.Globalization;

namespace Quantum
{
    class IoCConverter : BaseValueConverter<IoCConverter>
    {
        #region Value Converter Methods
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch((string)value)
            {
                case nameof(ApplicationViewModel):
                    return IoC.Get<ApplicationViewModel>();

                default:
                    Debugger.Break();
                    return null;
            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
