﻿using Quantum.Core;
using System;
using System.Diagnostics;
using System.Globalization;

namespace Quantum
{
    class SideMenuContentConverter : BaseValueConverter<SideMenuContentConverter>
    {
        #region Value Converter Methods
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch((SideMenuContent)value)
            {
                case SideMenuContent.Chat:
                    return new ChatListControl() {  };
                case SideMenuContent.Contacts:
                    return new FindUserControl() {  };

                default:
                    Debugger.Break();
                    return null;
            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
