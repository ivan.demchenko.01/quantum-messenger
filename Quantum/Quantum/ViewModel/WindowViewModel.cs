﻿
using Quantum.Core;
using System.Windows;
using System.Windows.Input;

namespace Quantum
{
    public class WindowViewModel : BaseViewModel
    {
        #region Private Members

        private Window mWindow;

        private bool _connectedToServer = false;

        private int mOuterMarginSize = 10;

        private int mWindowRadius = 10;

        private WindowDockPosition mDockPosition = WindowDockPosition.Undocked;

        

        #endregion

        #region Public Members 

        public bool ConnectedToServer
        {
            get
            {
                return _connectedToServer;
            }
            set
            {
                if (_connectedToServer == value)
                    return;

                _connectedToServer = value;

                OnPropertyChanged("ConnectedToServer");
            }
        }

        

        public double WindowMinimumWidth { get; set; } = 400;
        public double WindowMinimumHeight { get; set; } = 400;
        public int ResizeBorder { get; set; } = 6;

        public Thickness ResizeBorderThickness { get { return new Thickness(ResizeBorder + OuterMarginSize); } }

        public bool Borderless { get { return (mWindow.WindowState == WindowState.Maximized || mDockPosition != WindowDockPosition.Undocked); } }

        public int OuterMarginSize
        {
            get
            {
                return Borderless ? 0 : mOuterMarginSize;
            }
            set
            {
                mOuterMarginSize = value;
            }
        }

        public Thickness OuterMarginSizeThickness { get { return new Thickness(OuterMarginSize); } }

        public int WindowRadius
        {
            get
            {
                return mWindow.WindowState == WindowState.Maximized ? 0 : mWindowRadius;
            }
            set
            {
                mWindowRadius = value;
            }
        }

        public CornerRadius WindowCornerRadius
        {
             get { return new CornerRadius(WindowRadius); } 
        }

        public int TitleHeight { get; set; } = 50;

        public GridLength TitleHeightGridLenght { get { return new GridLength(TitleHeight + ResizeBorder); } }


        #endregion

        #region Constructor

        public WindowViewModel(Window window)
        {
            mWindow = window;

            mWindow.StateChanged += (sender, e) =>
            {
                OnPropertyChanged(nameof(ResizeBorderThickness));
                OnPropertyChanged(nameof(OuterMarginSize));
                OnPropertyChanged(nameof(OuterMarginSizeThickness));
                OnPropertyChanged(nameof(WindowRadius));
                OnPropertyChanged(nameof(WindowCornerRadius));
            };

            MinimizeCommand = new RelayCommand(() => mWindow.WindowState = WindowState.Minimized);
            MaximizeCommand = new RelayCommand(() => mWindow.WindowState ^= WindowState.Maximized);
            CloseCommand = new RelayCommand(() => mWindow.Close());

            mWindow.Loaded += ConnectToServer;
        }

        private void ConnectToServer(object sender, RoutedEventArgs e)
        {
            Client.client.OnConnected += () => ConnectedStatus(true);
            Client.client.OnError += (sender) => ConnectedStatus(false);

            Client.InitializeClient();
        }


        #endregion

        private void ConnectedStatus(bool status)
        {
            ConnectedToServer = status;
        }

        #region Commands

        public ICommand MinimizeCommand { get; set; }

        public ICommand MaximizeCommand { get; set; }

        public ICommand CloseCommand { get; set; }

        #endregion




    }
}
