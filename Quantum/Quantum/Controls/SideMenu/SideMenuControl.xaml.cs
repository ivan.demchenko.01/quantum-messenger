﻿using Quantum.Core;
using System.Windows.Controls;


namespace Quantum
{
    /// <summary>
    /// Логика взаимодействия для SideMenuControl.xaml
    /// </summary>
    public partial class SideMenuControl : UserControl
    {
        public SideMenuControl()
        {
            InitializeComponent();
        }

        private void GoToContactsSideControl(object sender, System.Windows.RoutedEventArgs e)
        {
            IoC.Application.GoToCurrentSideMenu(SideMenuContent.Contacts);

        }

        private void GoToMesageSideControl(object sender, System.Windows.RoutedEventArgs e)
        {
            IoC.Application.GoToCurrentSideMenu(SideMenuContent.Chat);

        }
    }
}
