﻿using Quantum.Core;
using System.Windows;
using System.Windows.Controls;

namespace Quantum
{
    /// <summary>
    /// Логика взаимодействия для ChatListControl.xaml
    /// </summary>
    public partial class ChatListControl : UserControl
    {
        public ChatListControl()
        {
            InitializeComponent();
            DataContext = new ChatListViewModel();

        }

        private void ChatListControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
           
        }
    }
}
