﻿using Ninject;
using Quantum.Core;

namespace Quantum
{
    public static class IoC
    {
        public static IKernel Kernel { get; private set; }

        public static ApplicationViewModel Application;

        public static SideMenuViewModel SideMenu;

        public static void Setup()
        {
            Kernel = new StandardKernel();

            BindViewModels();
        }

        private static void BindViewModels()
        {
            Kernel.Bind<ApplicationViewModel>().ToConstant(new ApplicationViewModel());
            Kernel.Bind<SideMenuViewModel>().ToConstant(new SideMenuViewModel());

            Application = IoC.Get<ApplicationViewModel>();
            SideMenu = IoC.Get<SideMenuViewModel>();
        }

        public static T Get<T>()
        {
            return Kernel.Get<T>();
        }
    }
}
