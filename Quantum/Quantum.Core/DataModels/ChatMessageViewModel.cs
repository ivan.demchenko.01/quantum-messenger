﻿namespace Quantum.Core
{
    public class ChatMessageViewModel
    {
        public string MessageToUsername { get; set; }
        public string Message { get; set; }
        public string MessageFromUserName { get; set; }
    }
}
