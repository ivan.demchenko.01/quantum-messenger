﻿namespace Quantum.Core
{
    /// <summary>
    /// The type of content in the side menu
    /// </summary>
    public enum SideMenuContent
    {
        /// <summary>
        /// A list of chat threads
        /// </summary>
        Chat = 1,

        /// <summary>
        /// A list of contacts
        /// </summary>
        Contacts = 2
    }
}