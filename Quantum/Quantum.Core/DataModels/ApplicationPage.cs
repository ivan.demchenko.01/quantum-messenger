﻿namespace Quantum.Core
{
    public enum ApplicationPage
    {
        Login = 0,
        Register = 1,
        Chat = 2
    }
}
