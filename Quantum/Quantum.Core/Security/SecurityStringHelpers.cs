﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Quantum.Core
{
    public static class SecurityStringHelpers
    {

        public static string Unsecure(this SecureString secureString)
        {
            if (secureString == null)
                return string.Empty;

            var unmanagedString = IntPtr.Zero;
            
            try
            {
                //Unsecured the password
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(secureString);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                //clean up memory
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

    }
}
