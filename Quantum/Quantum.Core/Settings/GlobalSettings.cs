﻿namespace Quantum.Core
{
    public static class GlobalSettings
    {
        public const string PORT = "3000";

        public const string HOST = "localhost";
    }
}
