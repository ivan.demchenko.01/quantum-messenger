﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace Quantum.Core
{
    public class RSA
    {

        private long n, p, q, m, e, d;

        public OpenKeys openKeys = new OpenKeys();

        public CloseKeys closeKeys = new CloseKeys();

        public RSA(int p, int q, int d)
        {
            this.n = p * q; 
            this.m = (p - 1) * (q - 1);
            this.d = d;
            this.e = FindE(d, this.m);// (e * d) % m = 1; (507882747 * 97843) % 936416128 = 1

            openKeys.e = (this.e);
            openKeys.n = (this.n);

            closeKeys.d = (this.d);
            closeKeys.n = (this.n);


        }

        private int FindE(long d, long m)
        {
            int i = 1;
            int e = 0;
            while (e == 0)
            {
                if ((i * d) % m == 1)
                    e = i;
                i++;
            }

            return e;
        }

        public string CryptString(string str, OpenKeys oKeys)
        {
            BigInteger bi;

            string temp = "";

            foreach (var ch in str)
            {
                bi = new BigInteger(Convert.ToInt32(ch));
                bi = BigInteger.Pow(bi, Convert.ToInt32(oKeys.e));
                bi = bi % oKeys.n;

                temp += bi.ToString() + ";" ;
            }

            return temp;
        }

        public string DecryptString(string listStr)
        {
            BigInteger bi;
            string str = "";

            List<int> temp = new List<int>();

            var array = listStr.Split(';');
            foreach (var item in array)
                if (item != "")
                    temp.Add(Convert.ToInt32(item));

            foreach (var chCode in temp)
            {
                bi = new BigInteger(chCode);
                bi = BigInteger.Pow(bi, Convert.ToInt32(this.d));
                bi = bi % this.n;

                str += (char)Convert.ToInt32(bi.ToString());
            }

            return str;
        }

        public List<int> CryptFile(string pathToFile)
        {
            BigInteger bi;

            var bytes = File.ReadAllBytes(pathToFile);

            List<int> tempList = new List<int>();

            foreach (var _byte in bytes)
            {
                bi = new BigInteger(_byte);

                bi = BigInteger.Pow(bi, Convert.ToInt32(this.e));
                bi = bi % this.n;

                tempList.Add(Convert.ToInt32(bi.ToString()));

            }

            return tempList;
        }

        public byte[] DecryptFile(string pathToFile)
        {
            BigInteger bi;

            List<byte> tempList = new List<byte>();

            var bytes = File.ReadAllBytes(pathToFile);

            foreach (var _byte in bytes)
            {
                bi = new BigInteger(_byte);
                bi = BigInteger.Pow(bi, Convert.ToInt32(this.d));
                bi = bi % this.n;


                tempList.Add((byte)Convert.ToInt32(bi.ToString()));

            }

            return tempList.ToArray();
        }



    }
}
