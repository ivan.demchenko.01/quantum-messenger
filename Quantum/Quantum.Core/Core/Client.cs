﻿using Newtonsoft.Json;
using SocketIOClient;
using SocketIOClient.Arguments;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Quantum.Core
{
    #region Helper clases

    public class OpenKeys
    {
        public long e, n;
    }

    public class CloseKeys
    {
        public long d, n;
    }

    public class User
    {
        public string Login;
        public string Password;
    }



    #endregion

    public static class Client
    {


        #region Public members

        public static SocketIO client = new SocketIO($"http://{GlobalSettings.HOST}:{GlobalSettings.PORT}");

        public static RSA rsaClient;

        public static OpenKeys serverOKeys = new OpenKeys();

        public static bool UserInitialized;

        public static bool ConnectedToServer;

        public static User User;

        #endregion

        #region Private members

        private static string token;

        private static List<string> keysListening = new List<string>();

        #endregion

        #region Methods

        #region Initialize client methods

        public static async Task InitializeClient()
        {
            rsaClient = InitRSAClient();

            client.Parameters = GetParameters();

            client.OnConnected += async () => await SendKeysToServer();
            client.OnConnected += () => { ConnectedToServer = true; };

            client.On("serverOpenKeys", async res => await SetupServerKeysAsync(res));

            User = new User();

            await TryConnectToServer(5);

        }

        private static async Task TryConnectToServer(int secondsWait)
        {
            while (true)
            {
                try
                {
                    await Task.Run(async () => await client.ConnectAsync());
                    break;
                }
                catch
                {
                    await Task.Delay(secondsWait * 1000);
                }
            }
        }

        private static async void Client_OnError(ResponseArgs obj)
        {
            //
        }

        private static RSA InitRSAClient()
        {
            PrimeFounder primeFounder = new PrimeFounder();
            int[] primeNumbers = primeFounder.GetPrimeNumbers();
            RSA rsaClient = new RSA(primeNumbers[0], primeNumbers[1], primeNumbers[2]);

            return rsaClient;
        }

        private static async Task SetupServerKeysAsync(ResponseArgs res)
        {
            dynamic data = JsonConvert.DeserializeObject(res.Text);

            serverOKeys.e = long.Parse(data["e"].ToString());
            serverOKeys.n = long.Parse(data["n"].ToString());
        }
        #endregion

        #region Key listening

        public static void AddKeyListening(string key)
        {
            if (!KeyListeningExists(key))
                keysListening.Add(key);
        }

        public static bool KeyListeningExists(string key)
        {
            if (keysListening.Exists(x => x == key))
                return true;
            return false;
        }

        #endregion

        #region User operations

        private static async Task SendKeysToServer()
        {
            await client.EmitAsync("clientOpenKeys", JsonConvert.SerializeObject(rsaClient.openKeys));
        }

        private static string GetToken()
        {
            Random rnd = new Random();
            return $"{rnd.Next(1, int.MaxValue)}{rnd.Next(1, int.MaxValue)}{rnd.Next(1, int.MaxValue)}";
        }

        private static string CryptSerializeUser(User u)
        {
            u.Login = rsaClient.CryptString(u.Login, serverOKeys);
            u.Password = rsaClient.CryptString(u.Password, serverOKeys);

            return JsonConvert.SerializeObject(u);
        }

        public static async void RegisterUser(User u)
        {
            await client.EmitAsync("registerUser", CryptSerializeUser(u));
        }

        public static async void FindUserByName(string userName)
        {
            await client.EmitAsync("registerUser", rsaClient.CryptString(userName, serverOKeys));
        }

        public static async void SendMessageToUser(ChatMessageViewModel message, OpenKeys keys)
        {
            message.Message = rsaClient.CryptString(message.Message, keys);

            await client.EmitAsync("sendMessageToUser", JsonConvert.SerializeObject(message));
        }




        #endregion

        private static Dictionary<string, string> GetParameters()
        {
            token = GetToken();
            return new Dictionary<string, string>
            {
                { "token", token }
            };
        }

        #endregion
    }
}
