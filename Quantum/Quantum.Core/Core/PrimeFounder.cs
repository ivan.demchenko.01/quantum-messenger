﻿using System;
using System.Collections.Generic;

namespace Quantum.Core
{
    public class PrimeFounder
    {

        public int[] GetPrimeNumbers()
        {
            int[] temp = { 0, 0 , 0};

            List<int> primeNumbers = new List<int>();

            Random rnd = new Random();

            int rndNumberMin = 15;
            int rndNumberMax = 100;

            for (int i = rndNumberMin; i < rndNumberMax; i++)
            {
                bool isPrime = true;

                for (int j = 1; j < i; j++)
                    if (i != j && j != 1)
                        if (i % j == 0)
                        {
                            isPrime = false;
                            break;
                        }

                if (isPrime)
                    primeNumbers.Add(i);
            }

            int pIndex = 0;
            int qIndex = 0;
            int dIndex = 0;

            do
            {
                pIndex = rnd.Next(0, primeNumbers.Count);
                qIndex = rnd.Next(0, primeNumbers.Count);
                dIndex = rnd.Next(0, primeNumbers.Count);
            }
            while (qIndex == pIndex || qIndex == dIndex || pIndex == dIndex);

            temp[0] = primeNumbers[pIndex];
            temp[1] = primeNumbers[qIndex];
            temp[2] = primeNumbers[dIndex];

            return temp;
        }

    }
}
