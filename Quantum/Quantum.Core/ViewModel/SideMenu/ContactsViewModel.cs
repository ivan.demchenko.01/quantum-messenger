﻿using Newtonsoft.Json;
using Quantum.Core;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Quantum
{
    public class ContactsViewModel : BaseViewModel
    {
        public ICommand FindUserCommand { get; set; }

        public string UserNameToFound { get; set; }

        private UserContactViewModel _contactItem;

        public UserContactViewModel ContactItem
        {
            get
            {
                return _contactItem;
            }
            set
            {
                if (_contactItem == value)
                    return;

                _contactItem = value;

                OnPropertyChanged(nameof(ContactItem));
            }
        }

        private string _test;

        public string Test { get; set; }

        private bool _contactVisible = true;

        public bool ContactVisible
        {
            get => _contactVisible;
            set
            {
                if (_contactVisible == value)
                    return;

                _contactVisible = value;

                OnPropertyChanged(nameof(ContactVisible));
            }
        }


        public ContactsViewModel()
        {
            FindUserCommand = new RelayCommand(async () => await FindUsers(UserNameToFound));

            if (!Client.KeyListeningExists("GetUserToClient"))
            {
                Client.AddKeyListening("GetUserToClient");

                Client.client.On("GetUserToClient", data => {
                    string JSON = data.RawText.Replace("42", "");
                    dynamic JSONDeserialized = JsonConvert.DeserializeObject(JSON);

                    var userData = JSONDeserialized[1].ToString();

                    dynamic test = JsonConvert.DeserializeObject(userData);

                    var deserializedUserData = JsonConvert.DeserializeObject(test[0].ToString());


                    string userName = deserializedUserData["Login"].ToString();

                    ContactVisible = true;

                    OnPropertyChanged(nameof(ContactVisible));

                    ContactItem = new UserContactViewModel(userName);

                });
            }

        }

        private async Task FindUsers(string userName)
        {
            //Find contact
            await Client.client.EmitAsync("GetUserByName", userName);
            

        }


    }
}
