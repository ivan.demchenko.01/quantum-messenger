﻿using System.Windows.Input;

namespace Quantum.Core
{
    public class UserContactViewModel : BaseViewModel
    {
        public string UserName { get; set; }

        public string Initials { get; set; }

        public ICommand OpenChatCommand { get; set; }

        public UserContactViewModel(string userName)
        {
            OpenChatCommand = new RelayCommand(() => OpenChat(userName));

            UserName = userName;
            Initials = UserName[0].ToString() + UserName[UserName.Length - 1].ToString();

            OnPropertyChanged(nameof(UserName));
        }

        public void OpenChat(string username)
        {
            IoC.Application.GoToPage(ApplicationPage.Chat, new ChatMessageListViewModel(username));
        }
    }
}
