﻿using Newtonsoft.Json;
using System.Security;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Quantum.Core
{
    public class RegisterViewModel : BaseViewModel
    {

        #region Private Members

        private string _errorExists = "false";

        public string ErrorExists
        {
            get
            {
                return _errorExists;
            }
            set
            {
                if (_errorExists == value)
                    return;

                _errorExists = value;

                OnPropertyChanged("ErrorExists");
            }
        }

        #endregion

        #region Public Members 

        public string Email { get; set; }
        public SecureString Password { get; set; }

        private bool _loginIsRunning;

        public bool LoginIsRunning 
        {
            get 
            {
                return _loginIsRunning; 
            }
            set
            {
                if (_loginIsRunning == value)
                    return;

                _loginIsRunning = value;

                OnPropertyChanged("LoginIsRunning");
            }
        }

        #endregion

        #region Commands

        public ICommand LoginNavigateCommand { get; set; }
        public ICommand RegisterCommand { get; set; }

        #endregion

        #region Constructor

        public RegisterViewModel()
        {
            LoginNavigateCommand = new RelayCommand(async () => await LoginNavigateAsync());
            RegisterCommand = new RelayParameterizedCommand(async (parameter) => await RegisterAsync(parameter));

            if (!Client.KeyListeningExists("registerConfirm"))
            {
                Client.AddKeyListening("registerConfirm");

                Client.client.On("registerConfirm", data => {

                    IoC.Get<ApplicationViewModel>().GoToPage(ApplicationPage.Chat);

                });
            }


            if (!Client.KeyListeningExists("registerError"))
            {
                Client.AddKeyListening("registerError");

                Client.client.On("registerError", data => {
                    ErrorExists = "true";
                });
            }

        }



        #endregion

        #region Methods

        private async Task RegisterAsync(object parameter)
        {
            string email = this.Email;
            string password = (parameter as IHavePassword).SecurePassword.Unsecure();

            await Client.client.EmitAsync("registerUser", new User() {
                Login = Client.rsaClient.CryptString(email, Client.serverOKeys),
                Password = Client.rsaClient.CryptString(password, Client.serverOKeys)
            });

            Task.Delay(1);
            //Do register
        }
        private async Task LoginNavigateAsync()
        {
            IoC.Get<ApplicationViewModel>().GoToPage(ApplicationPage.Login);
        }

        private async Task InitializeClient()
        {
            await Client.InitializeClient();
        }

        #endregion

    }
}
