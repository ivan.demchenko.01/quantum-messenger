﻿namespace Quantum.Core
{
    public class ChatMessageListItemViewModel
    {
        public string Message { get; set; }
        public string Time { get; set; }
        public bool SentByMe { get; set; }
    }
}
