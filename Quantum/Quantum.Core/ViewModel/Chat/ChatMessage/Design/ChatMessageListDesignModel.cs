﻿using System.Collections.Generic;

namespace Quantum.Core
{
    public class ChatMessageListDesignModel : ChatMessageListViewModel
    {
        public static ChatMessageListDesignModel Instance => new ChatMessageListDesignModel();

        public ChatMessageListDesignModel()
        {
            Items = new List<ChatMessageListItemViewModel>
            {

            };
        }
    }
}
