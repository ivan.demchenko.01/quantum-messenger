﻿namespace Quantum.Core
{
    public class ChatMessageListItemDesignModel : ChatMessageListItemViewModel
    {
        public static ChatMessageListItemDesignModel Instance => new ChatMessageListItemDesignModel();

        public ChatMessageListItemDesignModel()
        {
            Message = "Hi, its my message to you";
            Time = "13:55";
        }
    }
}
