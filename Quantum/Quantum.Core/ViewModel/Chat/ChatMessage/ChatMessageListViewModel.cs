﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Quantum.Core
{
    public class ChatMessageListViewModel : BaseViewModel
    {
        public List<ChatMessageListItemViewModel> Items { get; set; }

        public string DialogWithUsername { get; set; }

        public OpenKeys userOpenKeys { get; set; }

        public ICommand SendMessageCommand { get; set; }

        public string SendMessageText { get; set; }

        public ChatMessageListViewModel()
        {
            SendMessageCommand = new RelayCommand(async () => await SendMessage(SendMessageText));
        }

        public ChatMessageListViewModel(string username)
        {
            DialogWithUsername = username;

            GetOpenKeysByUserName();

            SendMessageCommand = new RelayCommand(async () => await SendMessage(SendMessageText));

            Items = new List<ChatMessageListItemViewModel>
            {
               
            };
        }



        public async Task GetOpenKeysByUserName()
        {
            await Client.client.EmitAsync("GetUserKeysByName", DialogWithUsername);

            if (!Client.KeyListeningExists("GetUserKeyToClient"))
            {
                Client.AddKeyListening("GetUserKeyToClient");

                Client.client.On("GetUserKeyToClient", data => {

                    string JSON = data.RawText.Replace("42", "");
                    dynamic JSONDeserialized = JsonConvert.DeserializeObject(JSON);
                    JSONDeserialized = JSONDeserialized[1];

                    var test = JSONDeserialized.ToString();

                    dynamic keysJSON = JsonConvert.DeserializeObject(test);

                    userOpenKeys = new OpenKeys()
                    {
                        e = Convert.ToInt32(keysJSON["e"]),
                        n = Convert.ToInt32(keysJSON["n"])
                    };
                    Task.Delay(1);
                });
            }
        }

        public async Task SendMessage(string Message)
        {
            var message = new ChatMessageViewModel() { Message = Message, MessageToUsername = DialogWithUsername, MessageFromUserName = Client.User.Login};

            Client.SendMessageToUser(message, userOpenKeys);
        }
    }
}
