﻿using System.Windows.Input;

namespace Quantum.Core
{
    public class ChatListItemViewModel : BaseViewModel
    {
        public string Message { get; set; }
        public string Name { get; set; }
        public string Initials { get; set; }
        public string ProfilePictureRGB { get; set; }
        public bool IsSelected { get; set; }

        public OpenKeys oKeys { get; set; }

        public ICommand OpenMessageCommand { get; set; }

        public ChatListItemViewModel()
        {
            OpenMessageCommand = new RelayCommand(OpenMessage);
        }

        public void OpenMessage()
        {
            IoC.Application.GoToPage(ApplicationPage.Chat, new ChatMessageListViewModel(Name));
        }

    }
}
