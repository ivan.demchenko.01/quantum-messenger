﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quantum.Core
{
    public class ChatListDesignModel : ChatListViewModel
    {
        public static ChatListDesignModel Instance => new ChatListDesignModel();

        public ChatListDesignModel()
        {
            GetChatList();
        }

        private void GetChatList()
        {
            if (Client.User == null)
                return;
            Client.client.EmitAsync("GetMessagesToClientByUserName", Client.User.Login);

            if (!Client.KeyListeningExists("GetMessagesToClient"))
            {
                Client.AddKeyListening("GetMessagesToClient");

                Client.client.On("GetMessagesToClient", data => {
                    string JSON = data.RawText.Replace("42", "");
                    dynamic JSONDeserialized = JsonConvert.DeserializeObject(JSON);

                    string messages = JSONDeserialized[1].ToString();

                    dynamic messagesJSON = JsonConvert.DeserializeObject(messages);

                    var chatList = new List<ChatListItemViewModel>();

                    foreach (var message in messagesJSON)
                    {
                        string msg = message.ToString();
                        dynamic messageDeserialized = JsonConvert.DeserializeObject(msg);

                        var test = messageDeserialized["message"].ToString();
                        if (!chatList.Exists(x => x.Name == messageDeserialized["MessageFrom"]))
                            chatList.Add(new ChatListItemViewModel()
                            {
                                Message = Client.rsaClient.DecryptString(messageDeserialized["message"].ToString()),
                                Name = messageDeserialized["messageFrom"].ToString()
                            });
                    }

                    Items = chatList;


                });
            }
        }
    }
}
