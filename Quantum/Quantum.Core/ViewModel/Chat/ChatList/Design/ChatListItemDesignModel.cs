﻿using System.Windows.Input;

namespace Quantum.Core
{
    public class ChatListItemDesignModel : ChatListItemViewModel
    {
        public static ChatListItemDesignModel Instance => new ChatListItemDesignModel();

        public ChatListItemDesignModel()
        {
            Initials = "AA";
            Name = "Luke";
            Message = "This chat app is awesome!";
            ProfilePictureRGB = "00b7ff";
        }
    }
}
