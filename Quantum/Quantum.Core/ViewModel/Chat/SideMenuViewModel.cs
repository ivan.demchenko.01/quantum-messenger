﻿using Quantum.Core;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Quantum
{
    public class SideMenuViewModel
    {
        public ICommand TestCommand { get; set; }
        public object Control
        {
            get; private set; // add Notification!
        }

        public SideMenuViewModel()
        {
            
            TestCommand = new RelayCommand(Test);
        }

        private void Test()
        {
            IoC.Application.GoToCurrentSideMenu(SideMenuContent.Chat);
        }
    }
}
