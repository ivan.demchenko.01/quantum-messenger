﻿using System.Windows.Input;

namespace Quantum.Core
{
    public class ApplicationViewModel : BaseViewModel
    {
        public BaseViewModel CurrentPageViewModel { get; set; }

        private ApplicationPage _currentPage = ApplicationPage.Login;

        private BaseViewModel _currentSideMenuViewModel;
        public BaseViewModel CurrentSideMenuViewModel
        {
            get
            {
                return _currentSideMenuViewModel;
            }
            set
            {
                if (_currentSideMenuViewModel == value)
                    return;

                _currentSideMenuViewModel = value;

                OnPropertyChanged(nameof(CurrentSideMenuViewModel));
            }
        }

        public ApplicationPage CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                if (_currentPage == value)
                    return;

                _currentPage = value;

                OnPropertyChanged("CurrentPage");
            }
        }

        private SideMenuContent _currentSideMenuContent = SideMenuContent.Contacts;

        public SideMenuContent CurrentSideMenuContent
        {
            get
            {
                return _currentSideMenuContent;
            }
            set
            {
                if (_currentSideMenuContent == value)
                    return;

                _currentSideMenuContent = value;

                OnPropertyChanged("CurrentSideMenuContent");
            }
        }

        public ICommand OpenContactsCommand { get; set; }
        public ICommand OpenChatCommand { get; set; }

        public bool SideMenuVisible { get; set; } = false;

        public void GoToPage(ApplicationPage page, BaseViewModel viewModel = null)
        {
            CurrentPage = page;
            CurrentPageViewModel = viewModel;
            SideMenuVisible = page == ApplicationPage.Chat;

            OnPropertyChanged(nameof(CurrentPage));
        }

        public void GoToCurrentSideMenu(SideMenuContent content)
        {
            CurrentSideMenuContent = content;

           
        }

        public ApplicationViewModel()
        {
            OpenChatCommand = new RelayCommand(OpenChat);
            OpenContactsCommand = new RelayCommand(OpenContacts);
        }

        private void OpenChat()
        {
            GoToCurrentSideMenu(SideMenuContent.Chat);
        }

        private void OpenContacts()
        {
            GoToCurrentSideMenu(SideMenuContent.Contacts);

        }



    }
}
