﻿using System.Security;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Quantum.Core
{
    public class LoginViewModel : BaseViewModel
    {

        #region Private Members

        private string _errorExists = "false";

        #endregion

        #region Public Members 

        public string ErrorExists
        {
            get
            {
                return _errorExists;
            }
            set
            {
                if (_errorExists == value)
                    return;

                _errorExists = value;

                OnPropertyChanged("ErrorExists");
            }
        }

        public string Email { get; set; }

        public SecureString Password { get; set; }

        private bool _loginIsRunning;

        public bool LoginIsRunning 
        {
            get 
            {
                return _loginIsRunning; 
            }
            set
            {
                if (_loginIsRunning == value)
                    return;

                _loginIsRunning = value;

                OnPropertyChanged("LoginIsRunning");
            }
        }

        #endregion

        #region Commands

        public ICommand LoginCommand { get; set; }
        public ICommand RegisterCommand { get; set; }

        #endregion

        #region Constructor

        public LoginViewModel()
        {
            LoginCommand = new RelayParameterizedCommand(async (parameter) => { await LoginAsync(parameter); });

            RegisterCommand = new RelayCommand(async () => await RegisterAsync());

            string loginConfirmKey = "loginConfirm",
                    loginErrorKey = "loginError";


            if (!Client.KeyListeningExists(loginConfirmKey))
            {
                Client.AddKeyListening(loginConfirmKey);

                Client.client.On(loginConfirmKey, data => {
                    
                    IoC.Get<ApplicationViewModel>().GoToPage(ApplicationPage.Chat);

                });
            }


            if (!Client.KeyListeningExists(loginErrorKey))
            {
                Client.AddKeyListening(loginErrorKey);

                Client.client.On(loginErrorKey, data => {
                    ErrorExists = "true";
                });
            }
        }

        #endregion

        #region Methods

        private async Task RegisterAsync()
        {
            //IoC.Get<ApplicationViewModel>().SideMenuVisible ^= true;

            //return;

            IoC.Get<ApplicationViewModel>().GoToPage(ApplicationPage.Register);
        }
        private async Task LoginAsync(object parameter)
        {
            LoginIsRunning = true;

            string email = this.Email;
            string password = (parameter as IHavePassword).SecurePassword.Unsecure();

            await Client.client.EmitAsync("loginUser", new User()
            {
                Login = Client.rsaClient.CryptString(email, Client.serverOKeys),
                Password = Client.rsaClient.CryptString(password, Client.serverOKeys)
            });

            Client.User.Login = Email;

            LoginIsRunning = false;

        }

        private async Task InitializeClient()
        {
            await Client.InitializeClient();
        }

        #endregion

    }
}
